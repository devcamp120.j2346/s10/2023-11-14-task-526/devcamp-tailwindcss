import BlogItem from "../components/BlogItem";
import data from "../data";

const Blog = () => {
    return (
        <>
            <div className="text-[#3056D3] text-center text-[18px] font-semibold leading-[24px] mt-[120px]">
                Our Blogs
            </div>
            <div className="text-[#2E2E2E] text-center text-[40px] font-bold leading-[45px] capitalize mt-[8px]">
                Our Recent News
            </div>
            <div className="text-[#637381] text-center text-[15px] font-normal leading-[25px] mt-[15px]">
                There are many variations of passages of Lorem Ipsum available <br/> but the majority have suffered alteration in some form.
            </div>
            <div className="grid grid-cols-3 mt-[80px] justify-items-center">
                {data.map((blog, index) => {
                    return <BlogItem key={index} blog={blog}/>
                })}
            </div>
        </>
    )
}

export default Blog;