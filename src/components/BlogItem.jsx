const BlogItem = ({blog}) => {
    return (
        <>
            <div class="flex flex-col max-w-[370px]">
                <img src={blog.image} className="rounded-md" alt="blog"/>
                <div className="text-white text-[12px] font-semibold leading-[24px] bg-[#3056D3] rounded-[5px] w-[115.868px] h-[31.158px] text-center mt-[34px]">{blog.time}</div>
                <div className="text-[#212B36] text-[24px] font-semibold leading-[32px] mt-[25px]">{blog.title}</div>
                <div className="text-[#637381] text-[16px] font-normal leading-[28px] mt-[15px]">{blog.description}</div>
            </div>
        </>
    )
}

export default BlogItem;